#!/bin/bash

export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm

# avoid rebuilding everything every time
export KBUILD_NOCMDDEP=1

make -j$(nproc) flash.bin
